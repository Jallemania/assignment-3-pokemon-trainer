import { Component, OnInit } from '@angular/core';
import { PokedexService } from './services/pokedex.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
constructor(
  private readonly userService: UserService,
  private readonly pokedexService: PokedexService
){}

ngOnInit(): void {
    if(this.userService.user){
      this.pokedexService.findAllPokemons();
    }
}
}
