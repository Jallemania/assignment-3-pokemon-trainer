import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule} from './app-routing.module';


import { AppComponent } from './app.component';
import { LoginPageComponent } from './pages/login/login-page.component';
import { TrainerPageComponent } from './pages/trainer/trainer-page.component';
import { PokedexPageComponent } from './pages/pokedex/pokedex-page.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { CatchButtonComponent } from './components/catch-button/catch-button.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LogoutButtonComponent } from './components/logout-button/logout-button.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    TrainerPageComponent,
    PokedexPageComponent,
    LoginFormComponent,
    CatchButtonComponent,
    PokemonListComponent,
    PokemonListItemComponent,
    NavbarComponent,
    LogoutButtonComponent
  ],
  imports: [
    BrowserModule, 
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
