import { Pokemon } from "./pokemon.model";

export interface User {
    id: string;
    username: string;
    pokemon: Pokemon[];
}
