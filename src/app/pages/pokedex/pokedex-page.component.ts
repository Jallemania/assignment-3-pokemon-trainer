import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokedexService } from 'src/app/services/pokedex.service';

@Component({
  selector: 'app-pokedex-page',
  templateUrl: './pokedex-page.component.html',
  styleUrls: ['./pokedex-page.component.css']
})

/**
 * Function for the pokedex page (catalog page). Contains and displays all the pokemon.
 */
export class PokedexPageComponent implements OnInit {

  get loading(): boolean {
    return this.pokedexService.loading;
  }

  get pokemon(): Pokemon[] {
    return this.pokedexService.pokemon;
  }

  get error(): string {
    return this.pokedexService.error;
  }

  constructor(
    private readonly pokedexService: PokedexService
  ) { }

  ngOnInit(): void {
    this.pokedexService.findAllPokemons();
  }

}
