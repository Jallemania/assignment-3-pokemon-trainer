import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.css']
})

/**
 * Function for the trainer page. It contains and displays the pokemons that the specific user has
 * caught.
 */
export class TrainerPageComponent implements OnInit {

  get user(): User | undefined{
    return this.userService.user;
  }

  get pokemon(): Pokemon[]{
    if(this.userService.user){
      return this.userService.user.pokemon;
    }

    return [];
  }

  constructor(
    private userService: UserService,
  ) { }

  ngOnInit(): void {
  }

}
