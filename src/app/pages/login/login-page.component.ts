import { Component,  } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})

/**
 * Function for the login. When a user logs in, they are redirected to the pokedex page.
 */
export class LoginPageComponent {

  constructor(private readonly router: Router) { }

  handleLogin(): void {
    this.router.navigateByUrl("/pokedex");
  }
}
