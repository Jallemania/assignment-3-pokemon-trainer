/**
 * Function to handle the addition and removal of a specific user from the session storage.
 */
export class StorageUtil{
    public static storageSave<T>(key: string, value: T): void {
        sessionStorage.setItem(key, JSON.stringify(value));
    }
    
    public static storageRead<T>(key: string): T | undefined {
        const storedValue = sessionStorage.getItem(key);
        try {
            if(storedValue){
                return JSON.parse(storedValue);
            }
    
            return undefined;
            
        } catch (error) {
            sessionStorage.removeItem(key);
            return undefined;
        }
    }

    public static storageRemove<T>(key: string): T | undefined {
        sessionStorage.removeItem(key);
        return 
    }
}
