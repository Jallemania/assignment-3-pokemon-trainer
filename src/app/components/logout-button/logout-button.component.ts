import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.css']
})

/**
 * Function for the logout button. On click the user is logged out, redirected to the login page 
 * and removed from the session storage. 
 */
export class LogoutButtonComponent implements OnInit {

  @Input()  user!: User;

  constructor(
    private readonly userService: UserService,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
  }

  onLogoutClick() {
    this.userService.toLogOut(this.user);
    this.router.navigateByUrl("/login");
  }
}