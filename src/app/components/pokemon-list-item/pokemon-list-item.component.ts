import { Component, Input, OnInit } from '@angular/core';
import { UrlHandlingStrategy } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})

/**
 * Helper function that contains one pokemon (item) from the Pokemon list fetched from the api.
 */
export class PokemonListItemComponent implements OnInit {

  @Input() pokemon!: Pokemon;
  
  constructor() { }

  ngOnInit(): void {

  }

}

