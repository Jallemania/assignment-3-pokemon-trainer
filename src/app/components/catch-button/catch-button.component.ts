import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { CaughtPokemonService } from 'src/app/services/caught-pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-catch-button',
  templateUrl: './catch-button.component.html',
  styleUrls: ['./catch-button.component.css']
})
/**
 * Function of the catch button on every pokemon. On click the specific pokemon is added
 * in the trainer's page, the trainer is updated in the trainer api and in the session storage. 
 */
export class CatchButtonComponent implements OnInit {

  public _loading: boolean = false;

  public isCaught: boolean = false;

  public pokeball: string = "./assets/images/pokeball.png"

  @Input() pokemonId: string = "";

  constructor(
    private readonly  userService: UserService,
    private readonly caughtPokemonService: CaughtPokemonService,
  ) { }

  ngOnInit(): void {
    this.isCaught = this.userService.inCaught(this.pokemonId);
  }

  onCatchClick(): void {
    this._loading = true;
    this.caughtPokemonService.addToCaught(this.pokemonId)
      .subscribe({
        next: (user: User) => {
          this._loading = false;
          this.isCaught = this.userService.inCaught(this.pokemonId);
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR: ", error.message);
          
        }
      })
  }
}

