import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})

/**
 * Function for the user service. A specific user is set and saved in the storage session. 
 * Also updates the trainer's caught pokemon array.
 */
export class UserService {

  private _user?: User;

  get user(): User | undefined{
    return this._user;
  }

  set user(user: User | undefined){
    StorageUtil.storageSave<User>(StorageKeys.user, user!);
    this._user = user;
  }
 
  constructor() {
    this._user = StorageUtil.storageRead<User>(StorageKeys.user);
  }

  public inCaught(pokemonId: string): boolean{
    if(!this._user) return false;
    return Boolean(this._user?.pokemon.find((pokemon: Pokemon) => pokemon.id === pokemonId));
  }

  public addToCaught(pokemon: Pokemon): void{
    if(this._user){

      this._user.pokemon.push(pokemon);
    }
  }

  public removeFromCaught(pokemonId: string): void{
    if(this._user){
      this._user.pokemon = this._user.pokemon.filter((pokemon: Pokemon) => pokemon.id !== pokemonId);
    }
  }

  public toLogOut(user:User){
    if(this._user){
     this._user = StorageUtil.storageRemove<User>(StorageKeys.user);
    }
  }
}
