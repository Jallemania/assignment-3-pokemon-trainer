import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap, } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { PokedexService } from './pokedex.service';
import { UserService } from './user.service';

const {apiKey, apiUsers} = environment;

@Injectable({
  providedIn: 'root'
})
export class CaughtPokemonService {

  constructor(
    private http: HttpClient,
    private readonly pokedexService: PokedexService,
    private readonly userService: UserService,
  ) { }

  /**
   * If there is a user, the pokemon is added to the caught pokemons and the api is updated. 
   * If it has already been caught it gets released and removed.
   */
  public addToCaught(pokemonId: string): Observable<User> {
    if(!this.userService.user) throw new Error("AddToCaught: There is no user");

    const user: User = this.userService.user;

    const pokemon: Pokemon | undefined = this.pokedexService.pokemonById(pokemonId);

    if(!pokemon) throw new Error("No pokemon with id:" + pokemonId);

    if(this.userService.inCaught(pokemonId)){
      this.userService.removeFromCaught(pokemonId);  
    }else{
      this.userService.addToCaught(pokemon);
    }

    const headers = new HttpHeaders({
      'content-type' : 'application/json',
      'x-api-key' : apiKey
    })

    return this.http.patch<User>(`${apiUsers}/${user.id}`, {
      pokemon: [...user.pokemon],
    }, {
      headers
    })
    .pipe(
      tap((updatedUser: User) =>{
        this.userService.user = updatedUser;
      })
    )
  }

}
