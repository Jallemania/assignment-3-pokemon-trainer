import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';

const {apiPokemons} = environment

@Injectable({
  providedIn: 'root'
})

/**
 * Function for the pokedex service. Gets all the pokemon from the api. 
 */
export class PokedexService {
  
  private _pokemon: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  get pokemon(): Pokemon[]{
    return this._pokemon;
  }

  get error(): string {
    return this._error;
  }

  
  get loading(): boolean {
    return this._loading;
  }
  
  
  constructor(private readonly http: HttpClient ) { }

  public getPokemonId(url: string): string {

    let words = url.split('/');
    let id = words[words.length - 2];

    return id;

}

  public findAllPokemons(): void{

    if(this._pokemon.length > 0 || this._loading){
      return;
    }

    this._loading = true;
    this.http.get<Pokemon[]>(apiPokemons)
      .pipe(
        finalize(() => {
          this._loading = false;
        }),
        map((x: any) => x.results)
      )
      .subscribe({
        next: (pokemon: Pokemon[]) =>{
          this._pokemon = pokemon;
          this._pokemon.map(pokemon => pokemon.id = this.getPokemonId(pokemon.url));
          this._pokemon.map(p => p.img = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${p.id}.png`)
          
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
          
        }
      })
  }


  public pokemonById(id: string): Pokemon | undefined {
    return this._pokemon.find((pokemon: Pokemon) => pokemon.id === id);
  }

}
