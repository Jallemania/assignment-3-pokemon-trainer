import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';


const { apiUsers, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private readonly http: HttpClient) { }
  
  /**
   * Function for the login service. If the user does not exist,
   * they are created in the api and then loged in.
   * If they exist, they also login.
   */
  public login(username: string): Observable<User> {
    return this.checkUsername(username)
    .pipe(
      switchMap((user: User | undefined) =>{
        if(user === undefined){
          return this.createUser(username);
        }

        return of(user);
      })
    )
  }



  //check if user
  private checkUsername(username: string): Observable<User | undefined> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
      .pipe(
        map((response: User[]) =>  response.pop())
      )
  }

  //if not user - create
  private createUser(username: string): Observable<User> {
    //user -> headers -> api key -> POST
    const user = {
      username,
      pokemon: []
    }

    const headers = new HttpHeaders({
      "Content-Type" : "application/json",
      "x-api-key" : apiKey
    })

    return this.http.post<User>(apiUsers, user, { headers })
  }

  //if user or created user - store
}
