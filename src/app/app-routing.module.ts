import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { PokedexPageComponent } from "./pages/pokedex/pokedex-page.component";
import { LoginPageComponent } from "./pages/login/login-page.component";
import { TrainerPageComponent } from "./pages/trainer/trainer-page.component";

const routes: Routes = [
    {
        path: "", // path = no '/'
        pathMatch: "full",
        redirectTo: "/login", // redirect = '/path'
    },
    {
        path: "login",
        component: LoginPageComponent,
    },
    {
        path: "pokedex",
        component: PokedexPageComponent,
        canActivate: [ AuthGuard ]
    },
    {
        path: "trainer",
        component: TrainerPageComponent,
        canActivate: [ AuthGuard ]
    }
]


@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ], //import modules
    exports: [
        RouterModule
    ] //expose modules
})
export class AppRoutingModule {}