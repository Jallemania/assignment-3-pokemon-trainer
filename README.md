# Assignment 3 - Pokemon Trainer

## Install
Clone the repository and run npm install. Then create a new environment.ts file and add the following 6 lines:

 ```
export const environment = {
    production: false,
    apiUsers: "https://mj-noroff-api.herokuapp.com/trainers",
    apiPokemons: "https://pokeapi.co/api/v2/pokemon?limit=150",
    apiKey: <Add your key here>
  };
```


This app uses an environment key for the trainer api. This needs to be set up for everything to work correctly. 

## Usage
This assignment builds a Pokémon Trainer web app using the Angular Framework. In this application one is able to browse different pokemon, add them and release them to and from their collection. 

## Contributors
Marcus Jarlevid and Sofia Vulgari
